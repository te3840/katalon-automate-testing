<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BUU change password</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9408cd92-654a-42e3-8870-55b09474342e</testSuiteGuid>
   <testCaseLink>
      <guid>7e367b72-3d64-48d0-92c2-0307e7a22238</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Failed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72500a87-ff58-4947-9517-9c943f3eb3ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed new password less than 8 characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7907970-216b-4719-aabd-8be966a4d658</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed new password do not same as re-new password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bb05d18-b5e1-40ab-b97e-d4f3a93ce001</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed new password do not have special character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b390b1a8-fe61-4f33-978d-02fb95e54fda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed new password do not have number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f9b35ef-78a8-40d6-af25-1d6e9c04720b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Failed new password do not have a-z or A-Z</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b064f906-99ad-49e6-b16c-b440f5cc974d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change password successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30e737c3-bdee-4c0c-b327-24ce6ee587e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
